package com.yusufabd.data;

/**
 * Created by yusufabd on 2/4/2018.
 */

public class AppConfig {
    //Base Urls

    public static final String OPEN_WEATHER_MAP_BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String FLICKR_BASE_URL = "https://api.flickr.com/services/";

    //API keys

    public static final String OPEN_WEATHER_MAP_API_KEY = "8fc2388b9b69d52e1d2781b6b180387e";
    public static final String FLICKR_KEY = "98740b2fbb180c51f47ea9eb45346f5d";
    public static final String FLICKR_SECRET = "75e8e3ebf767f0f5";

    //URL schemas

    public static final String OWM_ICONS_SCHEME = "http://openweathermap.org/img/w/{icon}.png";
    public static final String FLICKR_PHOTO_SCHEME = "https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_{size}.jpg";

    //Timeout values

    public static final long CONNECT_TIMEOUT = 30 * 1000;
    public static final long READ_TIMEOUT = 30 * 1000;

    //Flickr params

    public static final String FLICKR_PARAM_API_KEY = "api_key";
    public static final String FLICKR_PARAM_FORMAT = "format";
    public static final String FLICKR_PARAM_PER_PAGE = "per_page";
    public static final String FLICKR_PARAM_EXTRAS = "extras";
    public static final String FLICKR_PARAM_NO_JSON_CALLBACK = "nojsoncallback";

    //Flickr default values

    public static final String JSON_FORMAT = "json";
    public static final String AMOUNT_PER_PAGE = "10";
    public static final String EXTRA_URL_Q = "url_q";
    public static final String METHOD_PHOTO_SEARCH = "flickr.photos.search";

    //Open Weather Map params

    public static final String OWM_PARAM_APP_ID = "appId";
    public static final String OWM_PARAM_MODE = "mode";
    public static final String OWM_PARAM_UNITS = "units";

    //OWM values

    public static final String METRIC_UNITS = "metric";

    //Intent extras

    public static final String EXTRA_CITY_NAME = "extra_city_name";
    public static final String EXTRA_CITY_PHOTO = "extra_city_photo";
}

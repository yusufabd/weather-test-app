package com.yusufabd.data;

import android.util.Log;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class LogTool {

    static String LOG_TAG = "custom_log_tag";

    public static void log(String msg){
        Log.d(LOG_TAG, msg);
    }
}

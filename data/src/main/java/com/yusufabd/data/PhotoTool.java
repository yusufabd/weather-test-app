package com.yusufabd.data;

import com.yusufabd.data.models.searchPhoto.PhotoItem;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class PhotoTool {

    public static boolean isLandscape(PhotoItem item){
        int height = Integer.parseInt(item.getHeightQ()),
                width = Integer.parseInt(item.getWidthQ());
        return width > height;
    }

    public static String getOWMIcon(String iconName){
        return AppConfig.OWM_ICONS_SCHEME.replace("{icon}", iconName);
    }

    public static String getFlickrImage(PhotoItem item){
        String photoUrl = AppConfig.FLICKR_PHOTO_SCHEME
                .replace("{farm-id}", String.valueOf(item.getFarm()))
                .replace("{server-id}", item.getServer())
                .replace("{id}", item.getId())
                .replace("{secret}", item.getSecret())
                .replace("{size}", "b");

        LogTool.log("Generated URL: " + photoUrl);

        return photoUrl;
    }
}

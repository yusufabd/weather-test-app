package com.yusufabd.data.mappers;

import com.yusufabd.data.PhotoTool;
import com.yusufabd.data.models.currentWeather.CurrentWeatherResponse;
import com.yusufabd.domain.models.Weather;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class CurrentWeatherMapper implements Func1<CurrentWeatherResponse, Observable<Weather>> {
    @Override
    public Observable<Weather> call(CurrentWeatherResponse currentWeatherResponse) {
        double temp = currentWeatherResponse.getMain().getTemp();

        String iconName = currentWeatherResponse.getWeather().get(0).getIcon();
        String icon = PhotoTool.getOWMIcon(iconName);

        int timestamp = currentWeatherResponse.getDt();
        Weather weather = new Weather(temp, icon, timestamp);
        return Observable.just(weather);
    }
}

package com.yusufabd.data.mappers;

import com.yusufabd.data.PhotoTool;
import com.yusufabd.data.models.forecast.ForecastResponse;
import com.yusufabd.data.models.forecast.ListItem;
import com.yusufabd.domain.models.Weather;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class ForecastMapper implements Func1<ForecastResponse, Observable<List<Weather>>>{
    @Override
    public Observable<List<Weather>> call(ForecastResponse forecastResponse) {
        List<Weather> weatherList = new ArrayList<>();

        List<ListItem> list = forecastResponse.getList();
        for(ListItem item : list){
            double temp = item.getMain().getTemp();
            String icon = PhotoTool.getOWMIcon(item.getWeather().get(0).getIcon());
            int timestamp = item.getDt();

            weatherList.add(new Weather(temp, icon, timestamp));
        }

        return Observable.just(weatherList);
    }
}

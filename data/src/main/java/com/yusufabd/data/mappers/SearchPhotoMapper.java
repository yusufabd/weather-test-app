package com.yusufabd.data.mappers;

import com.yusufabd.data.PhotoTool;
import com.yusufabd.data.models.searchPhoto.PhotoItem;
import com.yusufabd.data.models.searchPhoto.Photos;
import com.yusufabd.data.models.searchPhoto.SearchByTagResponse;

import java.util.Random;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class SearchPhotoMapper implements Func1<SearchByTagResponse, Observable<String>> {
    @Override
    public Observable<String> call(SearchByTagResponse searchByTagResponse) {
        Random random = new Random();
        Photos photos = searchByTagResponse.getPhotos();
        PhotoItem item = photos.getPhoto().get(random.nextInt(photos.getPhoto().size()));

        for (PhotoItem photoItem : photos.getPhoto()) {
            if (PhotoTool.isLandscape(photoItem)){
                item = photoItem;
                break;
            }
        }

        String photoUrl = PhotoTool.getFlickrImage(item);
        return Observable.just(photoUrl);
    }
}

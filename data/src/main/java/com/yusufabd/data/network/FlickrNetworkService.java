package com.yusufabd.data.network;

import com.yusufabd.data.models.searchPhoto.SearchByTagResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by yusufabd on 2/5/2018.
 */

public interface FlickrNetworkService {

    @GET("rest/")
    Observable<SearchByTagResponse> searchByTag(
            @Query("method") String method,
            @Query("tags") String tags
    );
}

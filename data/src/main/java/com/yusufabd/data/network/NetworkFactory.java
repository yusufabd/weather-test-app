package com.yusufabd.data.network;

import com.yusufabd.data.AppConfig;

/**
 * Created by yusufabd on 10/2/2017.
 */

public class NetworkFactory {

    private static WeatherNetworkService sWeatherService;
    private static FlickrNetworkService sFlickrService;

    public static WeatherNetworkService getWeatherService(){
        if (sWeatherService == null) {
            sWeatherService = RetrofitDelegate.getWeatherRetrofit(AppConfig.OPEN_WEATHER_MAP_BASE_URL).create(WeatherNetworkService.class);
        }
        return sWeatherService;
    }

    public static FlickrNetworkService getFlickrService() {
        if (sFlickrService == null) {
            sFlickrService = RetrofitDelegate.getFlickrRetrofit(AppConfig.FLICKR_BASE_URL).create(FlickrNetworkService.class);
        }
        return sFlickrService;
    }
}
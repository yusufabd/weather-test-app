package com.yusufabd.data.network;

import com.yusufabd.data.AppConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by yusufabd on 10/2/2017.
 */

public class OkHttpDelegate {


    static OkHttpClient getFlickrClient(){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .connectTimeout(AppConfig.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(AppConfig.READ_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        HttpUrl url = chain.request().url()
                                .newBuilder()
                                .addQueryParameter(AppConfig.FLICKR_PARAM_API_KEY, AppConfig.FLICKR_KEY)
                                .addQueryParameter(AppConfig.FLICKR_PARAM_FORMAT, AppConfig.JSON_FORMAT)
                                .addQueryParameter(AppConfig.FLICKR_PARAM_PER_PAGE, AppConfig.AMOUNT_PER_PAGE)
                                .addQueryParameter(AppConfig.FLICKR_PARAM_EXTRAS, AppConfig.EXTRA_URL_Q)
                                .addQueryParameter(AppConfig.FLICKR_PARAM_NO_JSON_CALLBACK, "1")
                                .build();
                        Request request = chain.request().newBuilder().url(url).build();
                        return chain.proceed(request);
                    }
                })
//                .addInterceptor(new Interceptor() {
//                    @Override
//                    public Response intercept(Chain chain) throws IOException {
//                        Request request = chain.request();
//                        Response response = chain.proceed(request);
//                        ResponseBody body = response.body();
//                        String bodyString = body.string();
//                        bodyString = bodyString.substring(14, bodyString.length() - 2);
//                        MediaType contentType = response.body().contentType();
//                        ResponseBody newBody = ResponseBody.create(contentType, bodyString);
//                        return response.newBuilder().body(newBody).build();
//                    }
//                })
                .addInterceptor(loggingInterceptor)
                .build();
    }

    public static OkHttpClient getWeatherClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .connectTimeout(AppConfig.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(AppConfig.READ_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        HttpUrl url = chain.request().url()
                                .newBuilder()
                                .addQueryParameter(AppConfig.OWM_PARAM_APP_ID, AppConfig.OPEN_WEATHER_MAP_API_KEY)
                                .addQueryParameter(AppConfig.OWM_PARAM_UNITS, AppConfig.METRIC_UNITS)
                                .addQueryParameter(AppConfig.OWM_PARAM_MODE, AppConfig.JSON_FORMAT)
                                .build();
                        Request request = chain.request().newBuilder().url(url).build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(loggingInterceptor)
                .build();
    }
}
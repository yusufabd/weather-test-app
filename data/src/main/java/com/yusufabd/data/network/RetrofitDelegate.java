package com.yusufabd.data.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yusufabd on 10/2/2017.
 */

public class RetrofitDelegate {

    public static Retrofit getWeatherRetrofit(String baseUrl){
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(OkHttpDelegate.getWeatherClient())
                .build();
    }

    public static Retrofit getFlickrRetrofit(String baseUrl){
        return new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .baseUrl(baseUrl)
                    .client(OkHttpDelegate.getFlickrClient())
                    .build();
    }
}

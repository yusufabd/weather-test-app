package com.yusufabd.data.network;

import com.yusufabd.data.models.currentWeather.CurrentWeatherResponse;
import com.yusufabd.data.models.forecast.ForecastResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by yusufabd on 2/5/2018.
 */

public interface WeatherNetworkService {

    @GET("weather/")
    Observable<CurrentWeatherResponse> getCurrentWeather(@Query("q") String city);

    @GET("forecast/")
    Observable<ForecastResponse> getForecast(@Query("q") String city);

}

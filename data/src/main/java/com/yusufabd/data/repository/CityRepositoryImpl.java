package com.yusufabd.data.repository;

import com.yusufabd.data.AppConfig;
import com.yusufabd.data.mappers.CurrentWeatherMapper;
import com.yusufabd.data.mappers.ForecastMapper;
import com.yusufabd.data.mappers.SearchPhotoMapper;
import com.yusufabd.data.network.FlickrNetworkService;
import com.yusufabd.data.network.NetworkFactory;
import com.yusufabd.data.network.WeatherNetworkService;
import com.yusufabd.domain.models.City;
import com.yusufabd.domain.models.Weather;
import com.yusufabd.domain.repository.CityRepository;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class CityRepositoryImpl implements CityRepository{

    private FlickrNetworkService flickrService = NetworkFactory.getFlickrService();
    private WeatherNetworkService weatherService = NetworkFactory.getWeatherService();

    @Override
    public Observable<List<City>> getCityList() {
        List<City> cityList = new ArrayList<>();
        cityList.add(new City("Moscow", "Kremlin"));
        cityList.add(new City("Saint Petersburg", "The State Hermitage Museum"));
        cityList.add(new City("New York", "The Empire State Building"));
        cityList.add(new City("Los Angeles", "Los Angeles"));
        cityList.add(new City("Chicago", "Cloud Gate"));
        cityList.add(new City("Toronto", "Toronto"));
        cityList.add(new City("Vancouver", "Greater Vancouver"));
        cityList.add(new City("Manchester", "Manchester"));
        cityList.add(new City("London", "Big Ben"));
        cityList.add(new City("Liverpool", "Anfield Road"));
        cityList.add(new City("Paris", "Eiffel Tower"));
        cityList.add(new City("Barcelona", "Kamp Nou"));
        cityList.add(new City("Milan", "Milan"));
        cityList.add(new City("Berlin", "Berlin Wall"));
        cityList.add(new City("Sydney", "Sydney"));
        cityList.add(new City("Tokyo", "Tokyo"));
        cityList.add(new City("Seoul", "Seoul"));
        cityList.add(new City("Tashkent", "Tashkent"));
        cityList.add(new City("Dubai", "Dubai"));
        cityList.add(new City("Rio", "Cristo Redentor"));
        cityList.add(new City("Buenos Aires", "Buenos Aires"));
        return Observable.just(cityList);
    }

    @Override
    public Observable<String> findPhotoForCity(String cityTag) {
        return flickrService.searchByTag(AppConfig.METHOD_PHOTO_SEARCH, cityTag)
                .flatMap(new SearchPhotoMapper());
    }

    @Override
    public Observable<Weather> getCurrentWeather(String city) {
        return weatherService.getCurrentWeather(city)
                .flatMap(new CurrentWeatherMapper());
    }

    @Override
    public Observable<List<Weather>> getForecast(String city) {
        return weatherService.getForecast(city)
                .flatMap(new ForecastMapper());
    }
}

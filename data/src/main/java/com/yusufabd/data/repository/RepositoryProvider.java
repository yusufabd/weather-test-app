package com.yusufabd.data.repository;

import com.yusufabd.domain.repository.CityRepository;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class RepositoryProvider {

    private static CityRepository cityRepository;

    public static CityRepository getCityRepository() {
        if (cityRepository == null) {
            cityRepository = new CityRepositoryImpl();
        }
        return cityRepository;
    }
}

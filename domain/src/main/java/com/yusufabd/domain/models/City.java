package com.yusufabd.domain.models;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class City {

    private String name;
    private String tag;
    private String photo;

    private double temp = -200;
    private String icon;

    public City(String name, String tag) {
        this.name = name;
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getTemp() {
        return temp;
    }

    public int getTempInt() {
        return (int) temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}

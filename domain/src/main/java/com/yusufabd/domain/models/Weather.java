package com.yusufabd.domain.models;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class Weather {

    private double temp;
    private String icon;
    private int timestamp;

    public Weather() {
    }

    public Weather(double temp, String icon, int timestamp) {
        this.temp = temp;
        this.icon = icon;
        this.timestamp = timestamp;
    }

    public double getTemp() {
        return temp;
    }

    public int getTempInt() {
        Double d = temp;
        return d.intValue();
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public long getTimestampMillis(){
        return (long) timestamp * 1000;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}

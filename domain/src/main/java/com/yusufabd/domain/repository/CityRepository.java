package com.yusufabd.domain.repository;

import com.yusufabd.domain.models.City;
import com.yusufabd.domain.models.Weather;

import java.util.List;

import rx.Observable;

/**
 * Created by yusufabd on 2/5/2018.
 */

public interface CityRepository {
    Observable<List<City>> getCityList();
    Observable<String> findPhotoForCity(String cityTag);

    Observable<Weather> getCurrentWeather(String city);
    Observable<List<Weather>> getForecast(String city);
}

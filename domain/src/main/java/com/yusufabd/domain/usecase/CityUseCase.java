package com.yusufabd.domain.usecase;

import com.yusufabd.domain.models.City;
import com.yusufabd.domain.models.Weather;
import com.yusufabd.domain.repository.CityRepository;

import java.util.List;

import rx.Observable;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class CityUseCase {

    private CityRepository repository;
    private Observable.Transformer transformer;

    public CityUseCase(CityRepository repository, Observable.Transformer transformer) {
        this.repository = repository;
        this.transformer = transformer;
    }

    public Observable<List<City>> getCityList(){
        return repository.getCityList();
    }

    public Observable<String> findPhotoForCity(String cityTag){
        return repository.findPhotoForCity(cityTag).compose(transformer);
    }

    public Observable<Weather> getCurrentWeather(String city){
        return repository.getCurrentWeather(city).compose(transformer);
    }

    public Observable<List<Weather>> getForecast(String city){
        return repository.getForecast(city).compose(transformer);
    }
}

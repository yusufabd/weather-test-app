package com.yusufabd.weathertestapp.activity.base;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.yusufabd.weathertestapp.R;

/**
 * Created by yusufabd on 2/5/2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void enableHomeButton(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showMessage(String message) {
        new AlertDialog.Builder(this)
                .setCancelable(true)
                .setTitle(R.string.system)
                .setMessage(message)
                .create()
                .show();
    }

    @Override
    public void showMessage(int message) {
        new AlertDialog.Builder(this)
                .setCancelable(true)
                .setTitle(R.string.system)
                .setMessage(message)
                .create()
                .show();
    }

    @Override
    public void showLoading(boolean show) {

    }
}

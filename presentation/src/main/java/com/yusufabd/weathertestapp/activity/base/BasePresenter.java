package com.yusufabd.weathertestapp.activity.base;

import rx.functions.Action0;
import rx.functions.Action1;

/**
 * Created by yusufabd on 2/5/2018.
 */

public abstract class BasePresenter {

    private BaseView view;

    public Action1<Throwable> onError(){
        return new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                view.showLoading(false);
                view.showMessage(throwable.getMessage());
            }
        };
    }

    public Action0 onLoadingStarted(){
        return new Action0() {
            @Override
            public void call() {
                view.showLoading(true);
            }
        };
    }

    public Action0 onLoadingFinished(){
        return new Action0() {
            @Override
            public void call() {
                view.showLoading(false);
            }
        };
    }

    public void setView(BaseView view) {
        this.view = view;
    }
}

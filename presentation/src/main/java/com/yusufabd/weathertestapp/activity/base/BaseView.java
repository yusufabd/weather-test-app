package com.yusufabd.weathertestapp.activity.base;

/**
 * Created by yusufabd on 2/5/2018.
 */

public interface BaseView {
    void showMessage(String message);
    void showMessage(int message);
    void showLoading(boolean show);
}

package com.yusufabd.weathertestapp.activity.city;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.brandongogetap.stickyheaders.StickyLayoutManager;
import com.squareup.picasso.Picasso;
import com.yusufabd.weathertestapp.R;
import com.yusufabd.weathertestapp.activity.base.BaseActivity;
import com.yusufabd.weathertestapp.adapters.ForecastAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class CityActivity extends BaseActivity implements CityView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;

    @BindView(R.id.image_view_city)
    ImageView imageViewCity;

    @BindView(R.id.recycler_view_days)
    RecyclerView recyclerViewDays;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        enableHomeButton();

        CityPresenter presenter = new CityPresenter(this);
        presenter.init(getIntent());
    }

    @Override
    public void showCityData(String name, String photo) {
        setTitle(name);
        Picasso.with(this).load(photo).fit().into(imageViewCity);
    }

    @Override
    public void showWeatherData(ForecastAdapter adapter) {
        LinearLayoutManager manager = new StickyLayoutManager(this, adapter);
        manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewDays.setLayoutManager(manager);
        recyclerViewDays.setAdapter(adapter);
    }

    @Override
    public void showLoading(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }
}

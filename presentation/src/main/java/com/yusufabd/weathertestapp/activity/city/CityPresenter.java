package com.yusufabd.weathertestapp.activity.city;

import android.content.Intent;

import com.yusufabd.data.AppConfig;
import com.yusufabd.data.repository.RepositoryProvider;
import com.yusufabd.domain.models.Weather;
import com.yusufabd.domain.usecase.CityUseCase;
import com.yusufabd.weathertestapp.activity.base.BasePresenter;
import com.yusufabd.weathertestapp.adapters.ForecastAdapter;
import com.yusufabd.weathertestapp.tools.RxTools;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class CityPresenter extends BasePresenter{

    private CityView view;
    private CityUseCase useCase;

    private List<Object> items = new ArrayList<>();
    private ForecastAdapter adapter;

    public CityPresenter(CityView cityView) {
        view = cityView;
        useCase = new CityUseCase(RepositoryProvider.getCityRepository(), RxTools.async());
        setView(view);
    }

    public void init(Intent intent){
        String name = intent.getStringExtra(AppConfig.EXTRA_CITY_NAME);
        String photo = intent.getStringExtra(AppConfig.EXTRA_CITY_PHOTO);

        view.showCityData(name, photo);

        getForecast(name);

        adapter = new ForecastAdapter(items);
        view.showWeatherData(adapter);
    }

    void getForecast(String city){
        useCase.getForecast(city)
                .doOnSubscribe(onLoadingStarted())
                .doOnTerminate(onLoadingFinished())
                .subscribe(new Action1<List<Weather>>() {
                    @Override
                    public void call(List<Weather> weathers) {
                        List<Object> objects = WeatherListTool.getFullList(weathers);
                        items.addAll(objects);
                        adapter.notifyDataSetChanged();
                    }
                }, onError());
    }
}

package com.yusufabd.weathertestapp.activity.city;

import com.yusufabd.weathertestapp.activity.base.BaseView;
import com.yusufabd.weathertestapp.adapters.ForecastAdapter;

/**
 * Created by yusufabd on 2/5/2018.
 */

public interface CityView extends BaseView{
    void showCityData(String name, String photo);
    void showWeatherData(ForecastAdapter adapter);
}

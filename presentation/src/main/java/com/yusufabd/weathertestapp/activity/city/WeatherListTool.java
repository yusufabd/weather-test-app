package com.yusufabd.weathertestapp.activity.city;

import com.yusufabd.domain.models.Weather;
import com.yusufabd.weathertestapp.tools.DateTool;
import com.yusufabd.weathertestapp.tools.Header;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class WeatherListTool {

    public static List<Object> getFullList(List<Weather> weatherList){
        List<Object> objects = new ArrayList<>();
        Weather weather = weatherList.get(0);
        String firstDate = DateTool.getDate(weather.getTimestampMillis());
        Header header = new Header(firstDate);
        objects.add(header);

        for(Weather item : weatherList){
            Date date1 = new Date(weather.getTimestampMillis());
            Date date2 = new Date(item.getTimestampMillis());

            if (!DateTool.sameDay(date1, date2)){
                String date = DateTool.getDate(item.getTimestampMillis());
                objects.add(new Header(date));
            }

            objects.add(item);
            weather = item;
        }

        return objects;
    }
}

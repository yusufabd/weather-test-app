package com.yusufabd.weathertestapp.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.yusufabd.data.AppConfig;
import com.yusufabd.weathertestapp.R;
import com.yusufabd.weathertestapp.activity.base.BaseActivity;
import com.yusufabd.weathertestapp.activity.city.CityActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainView, SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.recycler_view_cities)
    RecyclerView recyclerViewCities;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    MainPresenter presenter;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        presenter = new MainPresenter(this);
        presenter.init();
    }

    @Override
    public void showCityList(RecyclerView.Adapter adapter) {
        recyclerViewCities.setLayoutManager(layoutManager);
        recyclerViewCities.setAdapter(adapter);
    }

    @Override
    public void showLoading(boolean show) {
        swipeRefreshLayout.setRefreshing(show);
    }

    @Override
    public void startCityActivity(String name, String photo) {
        Intent intent = new Intent(this, CityActivity.class);
        intent.putExtra(AppConfig.EXTRA_CITY_NAME,  name);
        intent.putExtra(AppConfig.EXTRA_CITY_PHOTO,  photo);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        presenter.onRefresh();
    }
}

package com.yusufabd.weathertestapp.activity.main;

import android.view.View;

import com.yusufabd.data.repository.RepositoryProvider;
import com.yusufabd.domain.models.City;
import com.yusufabd.domain.models.Weather;
import com.yusufabd.domain.usecase.CityUseCase;
import com.yusufabd.weathertestapp.activity.base.BasePresenter;
import com.yusufabd.weathertestapp.adapters.CityListAdapter;
import com.yusufabd.weathertestapp.tools.RecyclerItemClickListener;
import com.yusufabd.weathertestapp.tools.RxTools;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class MainPresenter extends BasePresenter{

    private MainView view;
    private CityUseCase useCase;
    private CityListAdapter adapter;

    private List<City> cityList = new ArrayList<>();

    public MainPresenter(MainView mainView) {
        view = mainView;
        useCase = new CityUseCase(RepositoryProvider.getCityRepository(), RxTools.async());
        setView(view);
    }

    public void init(){
        adapter = new CityListAdapter(cityList);
        adapter.setItemClickListener(new RecyclerItemClickListener() {
            @Override
            public void onItemClick(View targetView, int position) {
                City city = cityList.get(position);
                view.startCityActivity(city.getName(), city.getPhoto());
            }
        });
        view.showCityList(adapter);
        getCityList();
    }

    void getCityList(){
        useCase.getCityList()
                .doOnSubscribe(onLoadingStarted())
                .doOnTerminate(onLoadingFinished())
                .subscribe(new Action1<List<City>>() {
                    @Override
                    public void call(List<City> cities) {
                        cityList.clear();
                        cityList.addAll(cities);
                        adapter.notifyDataSetChanged();
                        getCurrentWeather();
                        loadCityBackgrounds();
                    }
                }, onError());
    }

    void loadCityBackgrounds(){
        for (final City city : cityList) {
            useCase.findPhotoForCity(city.getTag())
                    .subscribe(new Action1<String>() {
                        @Override
                        public void call(String s) {
                            city.setPhoto(s);
                            adapter.notifyItemChanged(cityList.indexOf(city));
                        }
                    }, onError());
        }
    }

    void getCurrentWeather(){
        for (final City city : cityList) {
            useCase.getCurrentWeather(city.getName())
                    .subscribe(new Action1<Weather>() {
                        @Override
                        public void call(Weather weather) {
                            city.setTemp(weather.getTemp());
                            city.setIcon(weather.getIcon());
                            adapter.notifyItemChanged(cityList.indexOf(city));
                        }
                    }, onError());
        }
    }

    public void onRefresh() {
        init();
    }
}

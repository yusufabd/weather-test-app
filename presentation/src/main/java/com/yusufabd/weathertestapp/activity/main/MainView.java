package com.yusufabd.weathertestapp.activity.main;

import android.support.v7.widget.RecyclerView;

import com.yusufabd.weathertestapp.activity.base.BaseView;

/**
 * Created by yusufabd on 2/5/2018.
 */

public interface MainView extends BaseView {
    void showCityList(RecyclerView.Adapter adapter);
    void startCityActivity(String name, String photo);
}

package com.yusufabd.weathertestapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yusufabd.domain.models.City;
import com.yusufabd.weathertestapp.R;
import com.yusufabd.weathertestapp.tools.RecyclerItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.ViewHolder>{

    private Context context;
    private List<City> items;
    private RecyclerItemClickListener itemClickListener;

    public CityListAdapter(List<City> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(inflater.inflate(R.layout.list_item_city, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        City cityItem = items.get(holder.getLayoutPosition());
        String temp = context.getString(R.string.format_temp, cityItem.getTempInt());

        if (cityItem.getTemp() != -200)
            holder.textViewWeather.setText(temp);

        holder.textViewCityName.setText(cityItem.getName());

        Picasso.with(context)
                .load(cityItem.getPhoto()).fit().into(holder.imageViewCityBackground);
        Picasso.with(context)
                .load(cityItem.getIcon()).fit().into(holder.imageViewIcon);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.image_view_city_bg)
        ImageView imageViewCityBackground;
        @BindView(R.id.text_view_city_name)
        TextView textViewCityName;
        @BindView(R.id.image_view_icon)
        ImageView imageViewIcon;
        @BindView(R.id.text_view_city_weather)
        TextView textViewWeather;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null && getLayoutPosition() < getItemCount()){
                        itemClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    public void setItemClickListener(RecyclerItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}

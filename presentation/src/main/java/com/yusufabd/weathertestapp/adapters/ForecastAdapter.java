package com.yusufabd.weathertestapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler;
import com.squareup.picasso.Picasso;
import com.yusufabd.weathertestapp.tools.Header;
import com.yusufabd.domain.models.Weather;
import com.yusufabd.weathertestapp.R;
import com.yusufabd.weathertestapp.tools.DateTool;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class ForecastAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyHeaderHandler{

    Context context;
    List<Object> items;
    int VIEW_TYPE_HEADER = 111, VIEW_TYPE_WEATHER = 112;

    public ForecastAdapter(List<Object> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        if (viewType == VIEW_TYPE_HEADER)
            return new HeaderHolder(inflater.inflate(R.layout.list_item_header, parent, false));

        return new WeatherHolder(inflater.inflate(R.layout.list_item_weather, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_WEATHER){
            WeatherHolder weatherHolder = (WeatherHolder) holder;
            Weather weather = (Weather) items.get(position);
            Picasso.with(context)
                    .load(weather.getIcon()).fit().into(weatherHolder.imageViewIcon);
            weatherHolder.textViewTemp.setText(context.getString(R.string.format_temp, weather.getTempInt()));
            weatherHolder.textViewTime.setText(DateTool.getTime(weather.getTimestampMillis()));
        }else {
            HeaderHolder headerHolder = (HeaderHolder) holder;
            Header header = (Header) items.get(position);
            headerHolder.textViewDate.setText(header.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (items.get(position) instanceof Header)
            return VIEW_TYPE_HEADER;

        return VIEW_TYPE_WEATHER;
    }

    @Override
    public List<?> getAdapterData() {
        return items;
    }

    class WeatherHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.image_view_icon)
        ImageView imageViewIcon;
        @BindView(R.id.text_view_temp)
        TextView textViewTemp;
        @BindView(R.id.text_view_time)
        TextView textViewTime;
        public WeatherHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.text_view_date)
        TextView textViewDate;
        public HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

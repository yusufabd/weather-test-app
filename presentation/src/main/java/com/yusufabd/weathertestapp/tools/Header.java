package com.yusufabd.weathertestapp.tools;

import com.brandongogetap.stickyheaders.exposed.StickyHeader;

/**
 * Created by yusufabd on 2/5/2018.
 */

public class Header implements StickyHeader{

    String title;

    public Header() {
    }

    public Header(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

package com.yusufabd.weathertestapp.tools;

import android.view.View;

/**
 * Created by yusufabd on 2/5/2018.
 */

public interface RecyclerItemClickListener {
    void onItemClick(View targetView, int position);
}

package com.yusufabd.weathertestapp.tools;

import android.support.annotation.NonNull;

import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by yusufabd on 10/5/2017.
 */

public class RxTools {
    private RxTools() {
    }

    @NonNull
    public static <T> Observable.Transformer<T, T> async() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> observable) {
                return observable
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    @NonNull
    public static <T> Observable.Transformer<T, T> async(@NonNull final Scheduler background,
                                                         @NonNull final Scheduler main) {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> observable) {
                return observable
                        .subscribeOn(background)
                        .observeOn(main);
            }
        };
    }
}
